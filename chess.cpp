/*
 еще не сделано:
 - превращение пешек в другие фигуры
 - запрет на рокировку когда король под боем
 - правило 50 ходов
 - правило 3 повторений позиции
 - кажется, нужно изменить отсечение
 */


#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <stdio.h>
#include <set>
#include <queue>
#include <stack>

#define mp make_pair
#define pb push_back
#define ll long long
#define ull unsigned ll

using namespace std;

//PLAYER1 и PLAYER2 - информация об игроках
//если значение = 0, игрок - живой человек
//если значение > 0, игрок - компьютер, а значение - его сила (глубина на которую совершается перебор)
const int PLAYER1 = 5;
const int PLAYER2 = 5;

//PRINT_MODE - режим вывода информации о ходах компьютера
//0 - вывести только ход
//1 - ход и оценка позиции
//2 - выводить еще и *, когда обдуман один ход стороны, которая ходит
//3 - выводить -, когда обдуман один ответный ход
//4 - выводить информацию обо всех рассмотренных ходах первой стороны
//5 - выводить информацию обо всех рассмотренных ходах первой стороны и ответах на них

const int PRINT_MODE = 6;

//RND_EFFECT - если true, то добавляем в оценку позиции эффект случайности
const bool RND_EFFECT = false;

const int oo = (int) 1e9;

vector <int> moves;

//массивы для быстрого получения всех ходов фигуры (в виде битмасок)
//1-я размерность массива - номер поля
//2-я размерность массива - битмаска горизонтали/вертикали/диагонали
ll hor[64][256]; //горизонтали
ll ver[64][256]; //вертикали
ll dia1[64][256]; //диагонали 1
ll dia2[64][256]; //диагонали 2

//битмаски, содержащие расположение фигур
ull wfigures; //все белые фигуры
ull bfigures; //все черные фигуры
ull figures; //горизонтальный битборд
ull figures_ver; //вертикальный битборд
ull figures_dia1; //диагональный битборд 1
ull figures_dia2; //диагональный битборд 2

ull wpawn_chop[64]; //массив масок, указывающих на поля, которые бьют белые пешки
ull bpawn_chop[64]; //массив масок, указывающих на поля, которые бьют черные пешки

ull knights_chop[64]; //массив масок, указывающих на поля, которые бьют кони
ull kings_chop[64]; //массив масок, указывающих на поля, которые бьют короли

//два вспомогательных массива, для получения битмаски из диагонального битборда
int shift_dia1[20]; //массив, определяющий сдвиг битмаски
int and_dia1[20]; //массив, определяющий сколько последних бит числа оставлять (с помощью операции побитового И)

//лучший ход, содержит номера полей (откуда и куда)
pair <ull, ull> best;

//стек вызовов функции альфа-бета отсечения
stack < pair <ull, ull> > mvs;

//начальная глубина просмотра
int start;

//номер сессии запуска функции альфа-бета отсечения (для отладки)
int session = 0;

//массивы преобразования номера поля из горизонтальной ориентации в другие
int to_ver[64]; // в вертикальную ориентацию
int to_dia1[64]; // в диагональную ориентацию 1
int to_dia2[64]; // в диагональную ориентацию 2

//массив весов для позиционной оценки (все фигуры, все поля)
int vals[1000][64];

//веса для позиционной оценки (черные пешки)
int bpawn_val[64] = {
    0, 0, 0, 0, 0, 0, 0, 0,
    4, 4, 4, 0, 0, 4, 4, 4,
    6, 8, 2, 10, 10, 2, 8, 6,
    6, 8, 12, 16, 16, 12, 8, 6,
    8, 12, 16, 24, 24, 16, 12, 8,
    12, 16, 24, 32, 32, 24, 16, 12,
    12, 16, 24, 32, 32, 24, 16, 12,
    0, 0, 0, 0, 0, 0, 0, 0
};
//веса для позиционной оценки (белые пешки)
int wpawn_val[64] = {
    0, 0, 0, 0, 0, 0, 0, 0,
    12, 16, 24, 32, 32, 24, 16, 12,
    12, 16, 24, 32, 32, 24, 16, 12,
    8, 12, 16, 24, 24, 16, 12, 8,
    6, 8, 12, 16, 16, 12, 8, 6,
    6, 8, 2, 10, 10, 2, 8, 6,
    4, 4, 4, 0, 0, 4, 4, 4,
    0, 0, 0, 0, 0, 0, 0, 0

};
//веса для позиционной оценки (белый король)
int wking_val[64] = {
    0, 0, -4, -10, -10, -4, 0, 0,
    -4, -4, -8, -12, -12, -8, -4, -4,
    -12, -16, -20, -20, -20, -20, -16, -12,
    -16, -20, -24, -24, -24, -24, -20, -16,
    -16, -20, -24, -24, -24, -24, -20, -16,
    -12, -16, -20, -20, -20, -20, -16, -12,
    -4, -4, -8, -12, -12, -8, -4, -4,
    0, 0, -4, -10, -10, -4, 0, 0
};

//веса для позиционной оценки (черный король)
int bking_val[64] = {
    0, 0, -4, -10, -10, -4, 0, 0,
    -4, -4, -8, -12, -12, -8, -4, -4,
    -12, -16, -20, -20, -20, -20, -16, -12,
    -16, -20, -24, -24, -24, -24, -20, -16,
    -16, -20, -24, -24, -24, -24, -20, -16,
    -12, -16, -20, -20, -20, -20, -16, -12,
    -4, -4, -8, -12, -12, -8, -4, -4,
    0, 0, -4, -10, -10, -4, 0, 0
};

//веса для позиционной оценки (черный конь)
int bknight_val[64] = {
    0, 4, 8, 10, 10, 8, 4, 0,
    4, 8, 16, 20, 20, 16, 8, 4,
    8, 16, 24, 28, 28, 24, 16, 8,
    10, 20, 28, 32, 32, 28, 20, 10,
    10, 20, 28, 32, 32, 28, 20, 10,
    8, 16, 24, 28, 28, 24, 16, 8,
    4, 8, 16, 20, 20, 16, 8, 4,
    0, 4, 8, 10, 10, 8, 4, 0
};
//веса для позиционной оценки (черный слон)
int bbishop_val[64] = {
    14, 14, 14, 14, 14, 14, 14, 14,
    14, 22, 18, 18, 18, 18, 22, 14,
    14, 18, 22, 22, 22, 22, 18, 14,
    14, 18, 22, 22, 22, 22, 18, 14,
    14, 18, 22, 22, 22, 22, 18, 14,
    14, 18, 22, 22, 22, 22, 18, 14,
    14, 22, 18, 18, 18, 18, 22, 14,
    14, 14, 14, 14, 14, 14, 14, 14
};
//веса для позиционной оценки (белый слон)
int wbishop_val[64] = {
    14, 14, 14, 14, 14, 14, 14, 14,
    14, 22, 18, 18, 18, 18, 22, 14,
    14, 18, 22, 22, 22, 22, 18, 14,
    14, 18, 22, 22, 22, 22, 18, 14,
    14, 18, 22, 22, 22, 22, 18, 14,
    14, 18, 22, 22, 22, 22, 18, 14,
    14, 22, 18, 18, 18, 18, 22, 14,
    14, 14, 14, 14, 14, 14, 14, 14
};
//веса для позиционной оценки (белый конь)
int wknight_val[64] = {
    0, 4, 8, 10, 10, 8, 4, 0,
    4, 8, 16, 20, 20, 16, 8, 4,
    8, 16, 24, 28, 28, 24, 16, 8,
    10, 20, 28, 32, 32, 28, 20, 10,
    10, 20, 28, 32, 32, 28, 20, 10,
    8, 16, 24, 28, 28, 24, 16, 8,
    4, 8, 16, 20, 20, 16, 8, 4,
    0, 4, 8, 10, 10, 8, 4, 0
};

//устанавливаем числовые значения для цветов и фигур
const int WHITE = 0;
const int BLACK = 1;
const int PAWN = 2;
const int KNIGHT = 4;
const int BISHOP = 8;
const int ROOK = 16;
const int QUEEN = 32;
const int KING = 64;

//получаем значения для фигур разных цветов
const int WHITE_PAWN = (WHITE | PAWN);
const int BLACK_PAWN = (BLACK | PAWN);
const int WHITE_KNIGHT = (WHITE | KNIGHT);
const int BLACK_KNIGHT = (BLACK | KNIGHT);
const int WHITE_KING = (WHITE | KING);
const int BLACK_KING = (BLACK | KING);
const int WHITE_BISHOP = (WHITE | BISHOP);
const int BLACK_BISHOP = (BLACK | BISHOP);
const int WHITE_ROOK = (WHITE | ROOK);
const int BLACK_ROOK = (BLACK | ROOK);
const int WHITE_QUEEN = (WHITE | QUEEN);
const int BLACK_QUEEN = (BLACK | QUEEN);

ll cnt_ab = 0;
ll cnt_chop = 0;
ll cnt_ab_chop = 0;

//массив битмасок для всех фигур
ull fmasks[1000];

//массив весов всех фигур
int w[1100];
//представление доски
int field[64];
//информация о рокировке - четыре бита установлены в 1, каждый из них соответствует своей рокировке, которая пока возможна
pair <int,int> castle = mp(15,0);
//оценка позиции
int mark = 0;

//маска, 8 младших битов которой установлены в 1, нужна для получения маски горизонтали (вертикали, диагонали) при работе с битбордами
ull MASK = 255;

//значения, необходимые для функции frombb
const ull MASK2 = 65535;
int decode[50000];

//функция преобразования горизонтального битборда в вертикальный

ull conv_to_ver(ull x) {
    ull x2 = 0;
    for (int i = 0; i < 64; i++)
        if (x & (1ull << i)) x2 += (1ull << to_ver[i]);
    return x2;
}

//функция преобразования горизонтального битборда в диагональный 1

ull conv_to_dia1(ull x) {
    ull x2 = 0;
    for (int i = 0; i < 64; i++)
        if (x & (1ull << i)) x2 += (1ull << to_dia1[i]);
    return x2;
}

//функция преобразования горизонтального битборда в диагональный 2

ull conv_to_dia2(ull x) {
    ull x2 = 0;
    for (int i = 0; i < 64; i++)
        if (x & (1ull << i)) x2 += (1ull << to_dia2[i]);
    return x2;
}


//функция для проверки правильности позиции после применения функций make_move или cancel_move

bool check_correct() {

    bool z = true;

    for (int i = 0; i < 64; i++)
        if (field[i] > 0) {
            if ((field[i] % 2 == 0) && (!(wfigures & (1ull << i)))) z = false;
            if ((field[i] % 2 == 1) && (!(bfigures & (1ull << i)))) z = false;
        } else if ((bfigures & (1ull << i)) || (wfigures & (1ull << i))) z = false;

    return z;
}

//функция, которая преобразовывает номер позиции в формат битборда

ull tobb(int x) {
    return 1ull << x;
}

//функция, возвращающая номер позиции из номера в формате битборда
//степени числа 2 препросчитаны и для маленьких значений мы можем получить их за O(1) из массива decode
//если значение большое (больше 16), то число сдвигается и снова происходит обращение к массиву decode

int frombb(ull bb) {

    ull ans = 0;

    ans = (bb >> 48) & MASK2;
    if (ans) return decode[ans] + 48;

    ans = (bb >> 32) & MASK2;
    if (ans) return decode[ans] + 32;

    ans = (bb >> 16) & MASK2;
    if (ans) return decode[ans] + 16;

    return decode[bb & MASK2];

}

//преобразование хода из "человеческого формата" в формат битборда
//человеческий формат, например: s1 = "e2"; s2 = "e4";

pair <ull, ull> move_to_bb(string s1, string s2) {

    return mp(tobb((7 - (s1[1] - '1'))*8 + s1[0] - 'a'), tobb((7 - (s2[1] - '1'))*8 + s2[0] - 'a'));

}

//преобразование хода в формат для вывода на экран
string bb_to_str(pair <ull,ull> move) {
    
    if (move.first == 100 || move.first == 300) return "O-O";
    if (move.first == 200 || move.first == 400) return "O-O-O";
    
    string s = "";
    char ch = 'a';
    ch += frombb(move.first)%8;
    s += ch;
    ch = '1';
    ch += (7-frombb(move.first)/8);
    s += ch;
    s += '-';
    ch = 'a';
    ch += frombb(move.second)%8;
    s += ch;
    ch = '1';
    ch += (7-frombb(move.second)/8);
    s += ch;
    
    return s;
}


//устанавливаем значения весов фигур и их позиций

void init_weights() {

    w[0] = 0;


    //пешки
    w[WHITE_PAWN] = 1000;
    w[BLACK_PAWN] = -1000;

    //кони
    w[WHITE_KNIGHT] = 3000;
    w[BLACK_KNIGHT] = -3000;

    //слоны
    w[WHITE_BISHOP] = 3000;
    w[BLACK_BISHOP] = -3000;

    //ладьи
    w[WHITE_ROOK] = 5000;
    w[BLACK_ROOK] = -5000;

    //ферзи
    w[WHITE_QUEEN] = 9000;
    w[BLACK_QUEEN] = -9000;

    //короли
    w[WHITE_KING] = 300000;
    w[BLACK_KING] = -300000;

    //для черных фигур устанавливаем отрицательную позиционную оценку
    for (int i = 0; i < 64; i++) {
        bpawn_val[i] = -bpawn_val[i];
        bknight_val[i] = -bknight_val[i];
        bking_val[i] = -bking_val[i];
        bbishop_val[i] = -bbishop_val[i];
    }

    //заполняем массив vals
    for (int i = 0; i < 64; i++) {
        vals[WHITE_PAWN][i] = wpawn_val[i];
        vals[BLACK_PAWN][i] = bpawn_val[i];
        vals[WHITE_KNIGHT][i] = wknight_val[i];
        vals[BLACK_KNIGHT][i] = bknight_val[i];
        vals[WHITE_KING][i] = wking_val[i];
        vals[BLACK_KING][i] = bking_val[i];
        vals[WHITE_BISHOP][i] = wbishop_val[i];
        vals[BLACK_BISHOP][i] = bbishop_val[i];
    }

}

//вспомогательная функция для вывода на экран битборда

void printbb(ull bb) {
    for (ull i = 0; i < 64; i++) {

        cout << (((1ull << i)&(bb)) > 0);
        if (i % 8 == 7) cout << "\n";

    }
}

//вывод доски в "человеческом формате"

void printfield() {
    for (ull i = 0; i < 64; i++) {
        cout << "|";
        if (field[i] == 0) {
            if (((i / 8 + i % 8) % 2) == 1)
                cout << " ";
            else cout << " ";
        } else
            if (field[i] == 2) cout << "♙";
        else
            if (field[i] == 3) cout << "♟";
        else
            if (field[i] == 4) cout << "♘";
        else
            if (field[i] == 5) cout << "♞";
        else
            if (field[i] == 64) cout << "♔";
        else
            if (field[i] == 65) cout << "♚";
        else
            if (field[i] == WHITE_ROOK) cout << "♖";
        else
            if (field[i] == BLACK_ROOK) cout << "♜";
        else
            if (field[i] == WHITE_BISHOP) cout << "♗";
        else
            if (field[i] == BLACK_BISHOP) cout << "♝";
        else
            if (field[i] == WHITE_QUEEN) cout << "♕";
        else
            if (field[i] == BLACK_QUEEN) cout << "♛";
        else
            cout << "" << field[i] << "";
        if (i % 8 == 7) cout << "|\n";


    }
    cout << "----\n";
}

//функция, которая производит ход, обновляя все битборды и всю информацию о доске

pair <int,int>  make_move(pair <ull, ull> move, int from) {
    
    //получаем номера полей, куда и откуда сделан ход
    int scnd = frombb(move.second);
    int fst = frombb(move.first);
    
    if (((field[fst] == WHITE_PAWN) ||(field[fst] == BLACK_PAWN)) && (field[scnd] == 0) && (abs(fst-scnd) != 8) && (abs(fst-scnd) != 16)) {
        
        castle.second = 0;
        
        if (field[fst] == WHITE_PAWN) {
            make_move(mp(tobb(scnd+8),move.second),111);
            make_move(move,222);
            return mp(BLACK_PAWN,3);
        } else {
            make_move(mp(tobb(scnd-8),move.second),111);
            make_move(move,222);
            return mp(WHITE_PAWN,4);
        }
        
        
    }

    int pawn_to_figure = 0;

    //рокировки рассматриваем отдельно
    //рокировка белых (короткая)
    if (move.first == 100) {
        castle.first &= 15 - 3;
        make_move(mp(tobb(60), tobb(62)), 666);
        make_move(mp(tobb(63), tobb(61)), 666);
        return mp(0,0);
    }
    //рокировка белых (длинная)
    if (move.first == 200) {
        castle.first &= 15 - 3;
        make_move(mp(tobb(60), tobb(58)), 666);
        make_move(mp(tobb(56), tobb(59)), 666);
        return mp(0,0);
    }
    //рокировка черных (короткая)
    if (move.first == 300) {
        castle.first &= 15 - 12;
        make_move(mp(tobb(4), tobb(6)), 666);
        make_move(mp(tobb(7), tobb(5)), 666);
        return mp(0,0);
    }
    //рокировка черных (длинная)
    if (move.first == 400) {
        castle.first &= 15 - 12;
        make_move(mp(tobb(4), tobb(2)), 666);
        make_move(mp(tobb(0), tobb(3)), 666);
        return mp(0,0);
    }

    //определяем цвет стороны, которая сделала ход
    int col;
    if (move.first & BLACK) col = 1;
    else col = 0;

    //добавляем ход в стек ходов
    mvs.push(move);


    //обновляем информацию о рокировках белых
    if (fst == 56 || scnd == 56) castle.first &= 15 - 1;
    if (fst == 63 || scnd == 63) castle.first &= 15 - 2;
    if (fst == 60 || scnd == 60) castle.first &= 15 - 3;

    //обновляем информацию о рокировках черных
    if (fst == 0 || scnd == 0) castle.first &= 15 - 4;
    if (fst == 7 || scnd == 7) castle.first &= 15 - 8;
    if (fst == 4 || scnd == 4) castle.first &= 15 - 12;


    //обновляем вертикальный битборд
    figures_ver |= (1ull << to_ver[scnd]);
    figures_ver -= (1ull << to_ver[fst]);

    //обновляем диагональный битборд 1
    figures_dia1 |= (1ull << to_dia1[scnd]);
    figures_dia1 -= (1ull << to_dia1[fst]);

    //обновляем диагональный битборд 2
    figures_dia2 |= (1ull << to_dia2[scnd]);
    figures_dia2 -= (1ull << to_dia2[fst]);

    //выясняем, какую фигуру срубили
    int chopped = field[scnd];

    //если действительно была срублена фигура, то обновляем маски
    if (chopped > 0) {
        mark -= vals[chopped][scnd];
        fmasks[chopped] -= move.second;
    }
    
    if (((field[fst] == WHITE_PAWN) || (field[fst] == BLACK_PAWN)) && abs(scnd-fst) == 16) {
        castle.second = (scnd+fst)>>1;
    } else castle.second = 0;
    


    //обновляем оценку позиции
    mark -= vals[field[fst]][fst];
    mark += vals[field[fst]][scnd];


    //обновляем маски фигур
    fmasks[field[fst]] -= move.first;
    fmasks[field[fst]] += move.second;


    //обновляем доску
    field[scnd] = field[fst];
    field[fst] = 0;

    //вычитаем оценку срубленной фигуры
    mark -= w[chopped];
    
    if (field[scnd] == WHITE_PAWN && scnd < 8){
        pawn_to_figure = 1;        
        field[scnd] = WHITE_QUEEN;
        mark += w[WHITE_QUEEN]-w[WHITE_PAWN];
        
        fmasks[WHITE_PAWN] -= move.second;
        fmasks[WHITE_QUEEN] += move.second;
 
        
    }
    if (field[scnd] == BLACK_PAWN && scnd > 55){
        pawn_to_figure = 2;        
        field[scnd] = BLACK_QUEEN;
        mark += w[BLACK_QUEEN]-w[BLACK_PAWN];
        
        fmasks[BLACK_PAWN] -= move.second;
        fmasks[BLACK_QUEEN] += move.second;
        
    }   
    


    //обновляем маски для черных и белых фигур
    bfigures = (fmasks[BLACK_PAWN] | fmasks[BLACK_KNIGHT] | fmasks[BLACK_KING] | fmasks[BLACK_ROOK] | fmasks[BLACK_BISHOP] | fmasks[BLACK_QUEEN]);
    wfigures = (fmasks[WHITE_PAWN] | fmasks[WHITE_KNIGHT] | fmasks[WHITE_KING] | fmasks[WHITE_ROOK] | fmasks[WHITE_BISHOP] | fmasks[WHITE_QUEEN]);

    //обновляем общую маску
    figures = (wfigures | bfigures);

    return mp(chopped,pawn_to_figure);
}


//функция отмены хода

int cancel_move(pair <ull, ull> move, pair <int,int> chopped, pair <int,int> cstl, int old_mark) {

    //получаем номера полей, откуда и куда сделан ход
    int scnd = frombb(move.second);
    int fst = frombb(move.first);
    
    //восстанавливаем информацию о рокировке
    castle = cstl;
    
    mark = old_mark;        
    
    
    if (chopped.second == 3) {
        
        cancel_move(move,mp(BLACK_PAWN,0),castle,old_mark);
        cancel_move(mp(tobb(scnd+8),move.second),mp(0,0),castle,old_mark);
                
        return 0;
    }
    
    if (chopped.second == 4) {
        
        cancel_move(move,mp(WHITE_PAWN,0),castle,old_mark);
        cancel_move(mp(tobb(scnd-8),move.second),mp(0,0),castle,old_mark);
                
        return 0;
    }

    //рассматриваем, является ли ход рокировкой. Если да, то дробим его на два хода - ход ладьи и ход короля
    //короткая рокировка белых
    if (move.first == 100) {

        cancel_move(mp(tobb(63), tobb(61)), mp(0,0), cstl,old_mark);
        cancel_move(mp(tobb(60), tobb(62)), mp(0,0), cstl,old_mark);
        return 0;
    }
    //длинная рокировка белых
    if (move.first == 200) {

        cancel_move(mp(tobb(56), tobb(59)), mp(0,0), cstl,old_mark);
        cancel_move(mp(tobb(60), tobb(58)), mp(0,0), cstl,old_mark);
        return 0;
    }
    //короткая рокировка черных
    if (move.first == 300) {

        cancel_move(mp(tobb(7), tobb(5)), mp(0,0), cstl,old_mark);
        cancel_move(mp(tobb(4), tobb(6)), mp(0,0), cstl,old_mark);
        return 0;
    }
    //длинная рокировка черных
    if (move.first == 400) {

        cancel_move(mp(tobb(0), tobb(3)), mp(0,0), cstl,old_mark);
        cancel_move(mp(tobb(4), tobb(2)), mp(0,0), cstl,old_mark);
        return 0;
    }


    //определяем цвет стороны, которая ходит
    int col;
    if (move.second & BLACK) col = 1;
    else col = 0;


    //обновляем вертикальный битборд
    if (chopped.first == 0)
        figures_ver -= (1ull << to_ver[scnd]);
    figures_ver += (1ull << to_ver[fst]);

    //обновляем диагональный битборд 1
    if (chopped.first == 0)
        figures_dia1 -= (1ull << to_dia1[scnd]);
    figures_dia1 += (1ull << to_dia1[fst]);

    //обновляем диагональный битборд 2
    if (chopped.first == 0)
        figures_dia2 -= (1ull << to_dia2[scnd]);
    figures_dia2 += (1ull << to_dia2[fst]);


    //убираем ход из стека ходов
    mvs.pop();    


    //если была срублена фигура, восстанавливаем ее
    if (chopped.first > 0) {
        fmasks[chopped.first] += move.second;
    }
    
    //пересчитываем маски фигур
    fmasks[field[scnd]] += move.first;
    fmasks[field[scnd]] -= move.second;

    //меняем информацию на доске
    field[fst] = field[scnd];
    field[scnd] = chopped.first;
    
        
    if (chopped.second == 1){
        
        field[fst] = WHITE_PAWN;        
        
        fmasks[WHITE_PAWN] += move.first;
        fmasks[WHITE_QUEEN] -= move.first;
        
    }
    
    if (chopped.second == 2){
        
        field[fst] = BLACK_PAWN;                
        fmasks[BLACK_PAWN] += move.first;
        fmasks[BLACK_QUEEN] -= move.first;
        
    }    

    //восстанавливаем маски черных и белых фигур
    bfigures = (fmasks[BLACK_PAWN] | fmasks[BLACK_KNIGHT] | fmasks[BLACK_KING] | fmasks[BLACK_ROOK] | fmasks[BLACK_BISHOP] | fmasks[BLACK_QUEEN]);
    wfigures = (fmasks[WHITE_PAWN] | fmasks[WHITE_KNIGHT] | fmasks[WHITE_KING] | fmasks[WHITE_ROOK] | fmasks[WHITE_BISHOP] | fmasks[WHITE_QUEEN]);

    //получаем маску фигур
    figures = (wfigures | bfigures);


}

//генерирует все взятия всех фигур цвета col

vector < pair <ull, ull> > generate_agressive_moves(int col) {

    //содержит все ходы-взятия
    vector < pair <ull, ull> >agressive_moves;

    //получаем коды фигур для стороны, которая делает ход
    int OUR_QUEEN = QUEEN | col;
    int OUR_KING = KING | col;
    int OUR_KNIGHT = KNIGHT | col;
    int OUR_ROOK = ROOK | col;
    int OUR_BISHOP = BISHOP | col;
    int OUR_PAWN = PAWN | col;

    //получаем маску вражеских фигур
    ull enemy_figures = bfigures;

    if (col == BLACK) {
        enemy_figures = wfigures;
    }

    //генерируем ходы ферзей
    ull x = fmasks[OUR_QUEEN];

    //пока битмаска содержит информацию о ферзях извлекаем очередного ферзя
    while (x > 0) {

        ull p = (x & (-x)); //находим бит с очередным ферзем

        //генерируем все взятия по горизонтали
        int frbb = frombb(p);
        ull h = (frbb >> 3) << 3;
        ull msk = ((figures >> h) & MASK);

        ull x2 = (hor[frbb][msk] & enemy_figures);
        //просматриваем всех жертв
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            agressive_moves.pb(mp(p, p2));

        }

        //генерируем все взятия по вертикали
        ull v = ((frbb & 7) << 3);
        msk = ((figures_ver >> v) & MASK);
        x2 = (ver[frbb][msk] & enemy_figures);
        //просматриваем всех жертв
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            agressive_moves.pb(mp(p, p2));
        }

        //генерируем все взятия по диагонали 1
        v = (frbb & 7) + (frbb >> 3);
        msk = (figures_dia1 >> (shift_dia1[v])&(and_dia1[v]));
        x2 = dia1[frbb][msk] & enemy_figures;
        //просматриваем всех жертв
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            agressive_moves.pb(mp(p, p2));
        }

        //генерируем все взятия по диагонали 2
        v = (frbb & 7)-(frbb >> 3);
        msk = (figures_dia2 >> (shift_dia1[v + 7])&(and_dia1[v + 7]));
        msk = dia2[frbb][msk];

        x2 = msk & enemy_figures;
        //просматриваем всех жертв
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            agressive_moves.pb(mp(p, p2));
        }


        x ^= p; //устанавливаем в 0 бит с просмотренным ферзем
    }


    //генерируем ходы королей
    x = fmasks[OUR_KING];

    while (x > 0) {

        ull p = (x & (-x)); //получаем бит с королем

        ull p1 = (kings_chop[frombb(p)] & enemy_figures);
        //смотрим все взятия короля
        while (p1 > 0) {
            ull x2 = (p1 & (-p1));
            agressive_moves.pb(mp(p, x2));

            p1 ^= x2;

        }

        x ^= p; //сбрасываем бит, который просмотрели
    }

    //генерируем ходы ладей
    x = fmasks[OUR_ROOK];
    while (x > 0) {

        ull p = (x & (-x)); //извлекаем бит с очередной ладьей


        //генерируем все ходы по горизонтали
        int frbb = frombb(p);
        ull h = (frbb >> 3) << 3;
        ull msk = ((figures >> h) & MASK);

        //просматриваем все взятия
        ull x2 = (hor[frbb][msk] & enemy_figures);
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            agressive_moves.pb(mp(p, p2));

        }

        //генерируем все ходы по диагонали
        ull v = ((frbb & 7) << 3);
        msk = ((figures_ver >> v) & MASK);
        x2 = (ver[frbb][msk] & enemy_figures);

        //просматриваем все взятия
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            agressive_moves.pb(mp(p, p2));
        }


        x ^= p; // устанавливаем просмотренный бит в 0
    }


    //генерируем ходы слонов
    x = fmasks[OUR_BISHOP];
    int v;

    while (x > 0) {
        ull p = (x & (-x)); //получаем бит с очередным слоном
        int frbb = frombb(p);

        //просматриваем все ходы по диагонали 1
        v = (frbb & 7) + (frbb >> 3);
        ull msk = (figures_dia1 >> (shift_dia1[v])&(and_dia1[v]));
        ull x2 = dia1[frbb][msk] & enemy_figures;

        //просматриваем все взятия
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            agressive_moves.pb(mp(p, p2));
        }

        //просматриваем все ходы по диагонали 2
        v = (frbb & 7)-(frbb >> 3);
        msk = (figures_dia2 >> (shift_dia1[v + 7])&(and_dia1[v + 7]));
        msk = dia2[frbb][msk];

        x2 = msk & enemy_figures;
        //просматриваем все взятия
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            agressive_moves.pb(mp(p, p2));
        }

        x ^= p; //устанавливаем в 0 просмотренный бит

    }



    //генерируем ходы коней
    x = fmasks[OUR_KNIGHT];

    while (x > 0) {

        ull p = (x & (-x)); //получаем бит с очередным конем

        ull p1 = (knights_chop[frombb(p)] & enemy_figures);

        //просматриваем все взятия
        while (p1 > 0) {
            ull x2 = (p1 & (-p1));
            agressive_moves.pb(mp(p, x2));

            p1 ^= x2;

        }

        x ^= p; //устанавливаем в 0 просмотренный бит
    }


    //генерируем ходы пешек
    x = fmasks[OUR_PAWN];
    while (x > 0) {

        ull p = x & (-x); //выбираем бит с очередной пешкой\
        
        ull fr = p;
        ull pawn_chop;
        if (col == 0) pawn_chop = wpawn_chop[frombb(p)];
        else pawn_chop = bpawn_chop[frombb(p)];
        
        //рассматриваем отдельно взятие на проходе
        ull en_fig = enemy_figures;
        if (castle.second)
        en_fig ^= (1ull << castle.second);
        
        ull x2 = en_fig & pawn_chop;
        
        
        
        
        //просматриваем взятия пешки
        while (x2 > 0) {
            ull p2 = x2 & (-x2);
            agressive_moves.pb(mp(fr, p2));

            x2 ^= p2;
        }

        x ^= p; //устанавливаем в 0 просмотренный бит
    }


    return agressive_moves;

}


//функция для рассмотрения всех форсированных ходов, когда достигли глубины перебора
//Это делается для того чтобы избежать эффекта горизонта

int chops(int col, int alpha, int beta, int step) {
    
    
    cnt_chop ++;

    session++;

    //получаем оценку позиции, добавляем небольшое влияние случайности, дабы избежать всегда одинаковых игр компьютера
    int this_mark = mark;
    if (RND_EFFECT) this_mark += rand() % 11 - 5;
    if (col == BLACK) this_mark = -mark;

    //Если поставили мат
    if (this_mark > 200000 || this_mark < -200000) {
        if (this_mark > 200000) return 400000 + step;
        else return -400000 - step;
    }

    if (this_mark > alpha) alpha = this_mark;

    vector < pair <ull, ull> > agressive_moves;

    int maxx = this_mark;

    agressive_moves = generate_agressive_moves(col);

    //перебираем все взятия
    for (int i = 0; i < agressive_moves.size(); i++) {

        //альфа-бета отсечение
        if (alpha >= beta) break;

        pair <int,int> cstl = castle;
        int old_mark = mark;
        pair <int,int> chopped = make_move(agressive_moves[i], 1);

        int x = -chops(col^1, -beta, -alpha, step + 1);

        if (x > alpha) alpha = x;
        if (x > maxx) maxx = x;
        cancel_move(agressive_moves[i], chopped, cstl,old_mark);

    }



    return maxx;
}


//функция, отвечающая за перебор с альфа-бета отсечением

int alphabeta(int col, int d, int alpha, int beta, int type) {


    cnt_ab++;
    
    session++;

    int this_mark = mark;
    if (col == 1) this_mark = -this_mark;

    //если поставлен мат, выходим из перебора
    if (this_mark > 200000 || this_mark < -200000) {
        if (this_mark > 200000) return 400000 - d + start;
        else return -400000 + d - start;
    }

    pair <ull, ull> best_move;

    //если достигли конца глубины перебора, запускаем перебор форсированных вариантов
    if (d == 0) {
        cnt_ab_chop++;
        return chops(col, alpha, beta, 0);

    }

    //массив взятий
    vector < pair <ull, ull> > moves;
    moves = generate_agressive_moves(col);

    //массив тихих ходов
//    vector < pair <ull, ull> > moves;

    //получаем коды фигур для стороны, которая делает ход
    int OUR_QUEEN = QUEEN | col;
    int OUR_KING = KING | col;
    int OUR_KNIGHT = KNIGHT | col;
    int OUR_ROOK = ROOK | col;
    int OUR_BISHOP = BISHOP | col;
    int OUR_PAWN = PAWN | col;

    //получаем маску вражеских и наших фигур
    ull enemy_figures = bfigures;
    ull our_figures = wfigures;

    if (col == BLACK) {
        enemy_figures = wfigures;
        our_figures = bfigures;
    }


    //проверяем, можно ли сделать рокировки
    if (col == WHITE) {
        //короткая рокировка белых
        if ((castle.first & 2) && (!(figures & (1ull << 61))) && (!(figures & (1ull << 62)))) {
            moves.pb(mp(100, 100));
        }
        //длинная рокировка белых
        if ((castle.first & 1) && (!(figures & (1ull << 57))) && (!(figures & (1ull << 58)))&& (!(figures & (1ull << 59)))) {
            moves.pb(mp(200, 200));
        }
    } else {
        //короткая рокировка черных
        if ((castle.first & 8) && (!(figures & (1ull << 6))) && (!(figures & (1ull << 5)))) {
            moves.pb(mp(300, 300));
        }
        //длинная рокировка черных
        if ((castle.first & 4) && (!(figures & (1ull << 1))) && (!(figures & (1ull << 2)))&& (!(figures & (1ull << 3)))) {
            moves.pb(mp(400, 400));
        }
    }
    //Перебираем ходы ферзя
    ull x = fmasks[OUR_QUEEN];
    
    while (x > 0) {
        
        ull p = (x & (-x));//получаем бит с полем ферзя
        
        //генерируем ходы по горизонтали
        int frbb = frombb(p);
        ull h = (frbb >> 3) << 3;
        ull msk = ((figures >> h) & MASK);        

        ull x2 = (hor[frbb][msk]&(~figures));
        //перебираем все поля, на которые можно сходить
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            moves.pb(mp(p, p2));

        }

        //генерируем ходы по вертикали
        ull v = ((frbb & 7) << 3);
        msk = ((figures_ver >> v) & MASK);
        x2 = (ver[frbb][msk] & ~figures);
        //перебираем все поля, на которые можно сходить
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            moves.pb(mp(p, p2));
        }

        v = (frbb & 7) + (frbb >> 3);
        msk = (figures_dia1 >> (shift_dia1[v])&(and_dia1[v]));        
        x2 = dia1[frbb][msk] & (~figures);
        //перебираем все поля, на которые можно сходить
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            moves.pb(mp(p, p2));
        }



        v = (frbb & 7)-(frbb >> 3);
        msk = (figures_dia2 >> (shift_dia1[v + 7])&(and_dia1[v + 7]));
        msk = dia2[frbb][msk];


        x2 = (msk & ~figures);
        //перебираем все поля, на которые можно сходить
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            moves.pb(mp(p, p2));
        }

        //                cout << "\n";
        x ^= p;
    }


    //перебираем все ходы короля
    x = fmasks[OUR_KING];    
    while (x > 0) {

        ull p = (x & (-x)); //находим бит, содержащий поле короля

        ull p1 = kings_chop[frombb(p)] & enemy_figures;
        
        ull p2 = ((kings_chop[frombb(p)]^p1)&(~our_figures));

        //перебираем все поля, на которые можно сходить
        while (p2 > 0) {
            ull x2 = (p2 & (-p2));
            moves.pb(mp(p, x2));

            p2 ^= x2;

        }

        x ^= p;

    }
    
    
    //перебираем все ходы ладей
    x = fmasks[OUR_ROOK];
    while (x > 0) {
        
        ull p = (x & (-x));//находим бит с полем ладьи
        //перебираем ходы по горизонтали
        int frbb = frombb(p);
        ull h = (frbb >> 3) << 3;
        ull msk = ((figures >> h) & MASK);


        ull x2 = (hor[frbb][msk]&(~figures));
        //перебираем все поля, на которые можно сходить
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            moves.pb(mp(p, p2));
        }


        //перебираем ходы по вертикали
        ull v = ((frbb & 7) << 3);
        msk = ((figures_ver >> v) & MASK);
        x2 = (ver[frbb][msk] & ~figures);
        //перебираем все поля, на которые можно сходить
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            moves.pb(mp(p, p2));
        }

        x ^= p;//сбрасываем в 0 просмотренный бит
    }

    //перебираем все ходы слонов
    x = fmasks[OUR_BISHOP];
    int v;

    while (x > 0) {
        ull p = (x & (-x));//находим поле с очередным слоном
        int frbb = frombb(p);

        //перебираем ходы по диагонали 1
        v = (frbb & 7) + (frbb >> 3);
        ull msk = (figures_dia1 >> (shift_dia1[v])&(and_dia1[v]));
        ull x2 = dia1[frbb][msk] & (~figures);
        //перебираем все поля, на которые можно сходить
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            moves.pb(mp(p, p2));
        }


        //перебираем ходы по диагонали 2
        v = (frbb & 7)-(frbb >> 3);
        msk = (figures_dia2 >> (shift_dia1[v + 7])&(and_dia1[v + 7]));
        msk = dia2[frbb][msk];


        x2 = (msk & ~figures);
        //перебираем все поля, на которые можно сходить
        while (x2 > 0) {
            ull p2 = (x2 & (-x2));
            x2 ^= p2;
            moves.pb(mp(p, p2));
        }

        x ^= p;//сбрасываем в 0 просмотренный бит

    }

    //перебираем все ходы коней
    x = fmasks[OUR_KNIGHT];

    while (x > 0) {

        ull p = (x & (-x));//находим поле с очередным конем

        ull p1 = knights_chop[frombb(p)] & enemy_figures;
        ull p2 = ((knights_chop[frombb(p)]^p1)&(~our_figures));

        //перебираем все поля, на которые можно сходить
        while (p2 > 0) {

            ull x2 = (p2 & (-p2));
            moves.pb(mp(p, x2));

            p2 ^= x2;

        }

        x ^= p;

    }

    //перебираем все ходы пешек
    x = fmasks[OUR_PAWN];
    while (x > 0) {

        ull p = x & (-x);//смотрим очередную пешку

        ull fr = p;

        //сдвиг на 8 битов означает ход на одно поле по вертикали
        ull to = p >> 8;
        if (col == BLACK) to = p << 8;

        if (to != 0)
            if (!(figures & to)) {
                moves.pb(mp(fr, to));

                //еще один ход на одно поле (первым ходом, пешки могут пойти сразу на два поля)
                if (col == WHITE) to = to >> 8;
                if (col == BLACK) to = to << 8;

                if (col == WHITE)
                    if (to != 0)
                        if ((!(figures & to)) && (fr >= (1ull << 48))&& (fr <= (1ull << 55))) {

                            moves.pb(mp(fr, to));
                        }
                if (col == BLACK)
                    if (to != 0)
                        if ((!(figures & to)) && (fr >= (1ull << 8)) && (fr <= (1ull << 15))) {

                            moves.pb(mp(fr, to));
                        }

            }

        x ^= p; //сбрасываем в 0 просмотренный бит
    }


    int maxx = -oo;


    //сначала перебираем все взятия, поскольку среди них больше вероятность нахождения хорошего хода
    //обычные ходы просматриваем во вторую очередь, при этом отсортируем их сначала по статической оценке
    //это нужно, чтобы более хорошие ходы просматривались в первую очередь,
    //тем самым увеличивая количество отсечек
    vector < pair <int, int> > moves_ind;
    int alpha2 = alpha;
    for (int i = 0; i < moves.size(); i++) {

        pair<int,int> cstl = castle;
        int old_mark = mark;
                        
        pair <int,int> chopped = make_move(moves[i], 300);
        int tmark = mark;        
        if (col == BLACK) tmark = -tmark;
        if (d > 5 && type == 1) {
            tmark = -alphabeta(col^1, d - 5, -beta, -alpha,0);
//            alpha2 = max(alpha2,tmark);
            
        }
        
        moves_ind.pb(mp(tmark, -i));
        cancel_move(moves[i], chopped, cstl,old_mark);
    }

    sort(moves_ind.begin(), moves_ind.end());
    reverse(moves_ind.begin(), moves_ind.end());
    for (int i = 0; i < moves_ind.size(); i++)
        moves_ind[i].second = - moves_ind[i].second;




    for (int i = 0; i < moves.size(); i++) {
        
        

        //альфа-бета отсечение
        if (alpha >= beta) break;

        pair <int,int> cstl = castle;
        int old_mark = mark;
        pair <int,int> chopped = make_move(moves[moves_ind[i].second], 3);
        
        if (d == start) {
            
            if (PRINT_MODE >= 4) cout << "думаем над ходом: " << bb_to_str(moves[moves_ind[i].second]) << "\t" << 0.001*moves_ind[i].first << endl;           
        }
        if (d == start-1) {
            
            if (PRINT_MODE == 6) cout << "\tдумаем над ходом: " << bb_to_str(moves[moves_ind[i].second]) << "\t" << 0.001*moves_ind[i].first << endl;   
        }

        int x;
        if (moves_ind[i].first < -400000) x = -400000; else
        x = -alphabeta(col^1, d - 1, -beta, -alpha,type);

        if (x > maxx) maxx = x;

        if (x > alpha) {
            alpha = x;
            best_move = moves[moves_ind[i].second];
        }

        if (d == start) {
            if (PRINT_MODE >= 2 && PRINT_MODE < 4) cout << "*";            
            cout.flush();
            
            if (PRINT_MODE >= 4) cout << "ход: " << bb_to_str(moves[moves_ind[i].second]) << "\t оценка: " << 0.001*x << "\t" << 0.001*moves_ind[i].first << endl;
        }
        if (d == start - 1) {
            if (PRINT_MODE == 3) cout << "-";
            if (PRINT_MODE >= 5) cout << "\tход: " << bb_to_str(moves[moves_ind[i].second]) << "\t оценка: " << 0.001*x << "\t" << 0.001*moves_ind[i].first << endl;
            cout.flush();
        }
        if (d == start - 2) {
            
            if (PRINT_MODE == 6) cout << "\t\tход: " << bb_to_str(moves[moves_ind[i].second]) << "\t оценка: " << 0.001*x << "\t" << 0.001*moves_ind[i].first << endl;
            cout.flush();
        }

        cancel_move(moves[moves_ind[i].second], chopped, cstl,old_mark);

        if (d == start) best = best_move;

    }

    //пат
    if (moves.size() == 0) {
        return -250000;
    }


    return maxx;


}

//устанавливаем стартовые значения для всех переменных
void init() {
    for (int i = 0; i < 20; i++) {
        shift_dia1[i] = 0;
        and_dia1[i] = 0;
    }

    //устанавливаем препросчитанные значения сдвигов для перебора ходов по диагонали
    shift_dia1[0] = 0;    and_dia1[0] = 1;     shift_dia1[1] = 1;    and_dia1[1] = 3;    shift_dia1[2] = 3;    and_dia1[2] = 7;
    shift_dia1[3] = 6;    and_dia1[3] = 15;    shift_dia1[4] = 10;   and_dia1[4] = 31;   shift_dia1[5] = 15;   and_dia1[5] = 63;
    shift_dia1[6] = 21;   and_dia1[6] = 127;   shift_dia1[7] = 28;   and_dia1[7] = 255;  shift_dia1[8] = 36;   and_dia1[8] = 127;
    shift_dia1[9] = 43;   and_dia1[9] = 63;    shift_dia1[10] = 49;  and_dia1[10] = 31;  shift_dia1[11] = 54;  and_dia1[11] = 15;
    shift_dia1[12] = 58;  and_dia1[12] = 7;    shift_dia1[13] = 61;  and_dia1[13] = 3;   shift_dia1[14] = 63;  and_dia1[14] = 1;
    
    //заполняем массив для перевода номера поля в вертикальный битборд
    for (int i = 0; i < 64; i++) {
        int x1 = i / 8;
        int x2 = i % 8;
        to_ver[i] = x1 + x2 * 8;

    }
    //заполняем массив для перевода номера поля в диагональный битборд 1
    int cnt = 0;
    for (int i = 0; i <= 14; i++) {
        for (int j = 0; j <= 7; j++)
            for (int k = 0; k <= 7; k++)
                if (j + k == i) {
                    
                    to_dia1[j * 8 + k] = cnt;
                    cnt++;
                }
    }
    //заполняем массив для перевода номера поля в диагональный битборд 2
    cnt = 0;
    for (int i = -7; i <= 7; i++) {
        for (int j = 0; j <= 7; j++)
            for (int k = 0; k <= 7; k++)
                if (k - j == i) {                    
                    to_dia2[j * 8 + k] = cnt;
                    cnt++;
                }
    }
    
    //очищаем поле
    for (int i = 0; i < 64; i++)
        field[i] = 0;
    
    ull bb = 0;
    
    //препросчитываем все ходы из всех позиций при всех масках для горизонтали
    for (ull i = 0; i < 64; i++) {

        bb = 0;

        int x1 = i / 8;
        int x2 = i % 8;

        for (ull j = 0; j < 256; j++) {

            bb = 0;

            for (int k = x2 - 1; k >= 0; k--) {

                bb |= (1ull << (8 * x1 + k));
                if (j & (1ull << k)) break;
            }

            for (ull k = x2 + 1; k < 8; k++) {

                bb |= (1ull << (8 * x1 + k));
                if (j & (1ull << k)) break;
            }

            hor[i][j] = bb;

        }

    }
    
    
    vector <ull> v;  
    //препросчитываем все ходы из всех позиций при всех масках для вертикали
    for (ull i = 0; i < 64; i++) {

        bb = 0;

        int x1 = i / 8;
        int x2 = i % 8;

        for (ull j = 0; j < 256; j++) {

            bb = 0;

            for (int k = x1 - 1; k >= 0; k--) {

                bb |= (1ull << (8 * k + x2));
                if (j & (1ull << k)) break;
            }

            for (ull k = x1 + 1; k < 8; k++) {

                bb |= (1ull << (8 * k + x2));
                if (j & (1ull << k)) break;
            }

            ver[i][j] = bb;

            if (bb > 0) v.pb(bb);

        }

    }
   
    //препросчитываем все ходы коня
    vector < pair <int, int> > dir;  
    dir.pb(mp(-1, -2));    dir.pb(mp(-1, 2));    dir.pb(mp(1, -2));    dir.pb(mp(1, 2));
    dir.pb(mp(-2, -1));    dir.pb(mp(-2, 1));    dir.pb(mp(2, -1));    dir.pb(mp(2, 1));

    for (int i = 0; i < 64; i++) {
        ull x = 0;


        for (int j = 0; j < dir.size(); j++) {

            int x1 = i / 8 + dir[j].first;
            int x2 = i % 8 + dir[j].second;
            if (x1 >= 0 && x2 >= 0 && x1 <= 7 && x2 <= 7)
                x += (1ull << (x1 * 8 + x2));
        }

        knights_chop[i] = x;

    }

    dir.clear();

    //препросчитываем все ходы короля
    for (int i = -1; i <= 1; i++)
        for (int j = -1; j <= 1; j++)
            if (i != 0 || j != 0)
                dir.pb(mp(i, j));

    for (int i = 0; i < 64; i++) {
        ull x = 0;


        for (int j = 0; j < dir.size(); j++) {

            int x1 = i / 8 + dir[j].first;
            int x2 = i % 8 + dir[j].second;
            if (x1 >= 0 && x2 >= 0 && x1 <= 7 && x2 <= 7)
                x += (1ull << (x1 * 8 + x2));
        }

        kings_chop[i] = x;


    }
    
    //добавляем на поле белые пешки
    ull x = 0;
    for (int i = 0; i <= 7; i++) {

        x += tobb(6 * 8 + i);
        field[6 * 8 + i] = (WHITE_PAWN);

    }

    fmasks[WHITE_PAWN] = x;
 
    //добавляем на поле черные пешки
    x = 0;
    for (int i = 0; i <= 7; i++) {

        x += tobb(1 * 8 + i);
        field[1 * 8 + i] = (BLACK_PAWN);

    }   

    fmasks[BLACK_PAWN] = x;
    
 
    //добавляем на поле белых коней
    field[7 * 8 + 1] = (WHITE_KNIGHT);
    field[7 * 8 + 6] = (WHITE_KNIGHT);
    fmasks[WHITE_KNIGHT] = tobb(7 * 8 + 1) + tobb(7 * 8 + 6);
    
    //добавляем на поле черных коней
    field[1] = (BLACK_KNIGHT);
    field[6] = (BLACK_KNIGHT);
    fmasks[BLACK_KNIGHT] = tobb(0 * 8 + 1) + tobb(0 * 8 + 6);
   
    //добавляем на поле белого короля
    field[7 * 8 + 4] = (WHITE_KING);
    fmasks[WHITE_KING] = tobb(7 * 8 + 4);
    //добавляем на поле черного короля
    field[4] = (BLACK_KING);
    fmasks[BLACK_KING] = tobb(4);
    
    //добавляем на поле белые ладьи
    field[0] = (BLACK_ROOK);
    field[7] = (BLACK_ROOK);
    fmasks[WHITE_ROOK] = tobb(56) + tobb(63);

    //добавляем на поле черные ладьи
    field[56] = (WHITE_ROOK);
    field[63] = (WHITE_ROOK);
    fmasks[BLACK_ROOK] = tobb(0) + tobb(7);
    
    //добавляем на поле белых слонов
    field[58] = WHITE_BISHOP;
    field[61] = WHITE_BISHOP;
    fmasks[WHITE_BISHOP] = tobb(58) + tobb(61);

    //добавляем на поле черных слонов
    field[2] = BLACK_BISHOP;
    field[5] = BLACK_BISHOP;
    fmasks[BLACK_BISHOP] = tobb(2) + tobb(5);

    //добавляем на поле белого ферзя
    field[59] = WHITE_QUEEN;
    fmasks[WHITE_QUEEN] = tobb(59);
    //добавляем на поле черного ферзя
    field[3] = BLACK_QUEEN;
    fmasks[BLACK_QUEEN] = tobb(3);
   
    //получаем маску белых фигур
    wfigures = (fmasks[WHITE_PAWN] | fmasks[WHITE_KNIGHT] | fmasks[WHITE_KING] | fmasks[WHITE_ROOK] | fmasks[WHITE_BISHOP] | fmasks[WHITE_QUEEN]);

    //получаем маску черных фигур
    bfigures = (fmasks[BLACK_PAWN] | fmasks[BLACK_KNIGHT] | fmasks[BLACK_KING] | fmasks[BLACK_ROOK] | fmasks[BLACK_BISHOP] | fmasks[BLACK_QUEEN]);

    //получаем маску фигур
    figures = wfigures | bfigures;
    
    int i = 1;
    for (int j = 0; j <= 15; j++) {
        decode[i] = j;
        i *= 2;
    }
   
    //препросчитываем взятия белых пешек
    for (int i = 0; i < 64; i++) {

        wpawn_chop[i] = 0;
        if (i > 7) {
            if (i % 8 != 0)
                wpawn_chop[i] |= tobb(i) >> 9;
            if (i % 8 != 7)
                wpawn_chop[i] |= tobb(i) >> 7;
        }

        bpawn_chop[i] = 0;
        if (i < 56) {
            if (i % 8 != 7)
                bpawn_chop[i] |= tobb(i) << 9;
            if (i % 8 != 0)
                bpawn_chop[i] |= tobb(i) << 7;
        }

    }
    
    //получаем вертикальный и два диагональных битборда
    figures_ver = conv_to_ver(figures);
    figures_dia1 = conv_to_dia1(figures);
    figures_dia2 = conv_to_dia2(figures);
    
    
    //препросчитываем все ходы по диагонали 1
    for (int i = 0; i < 64; i++) {

        int x1 = i / 8;
        int x2 = i % 8;

        int cnt = 0;
        int num = 0;
        int all = min(x1 + x2, 7) - max(0, x1 + x2 - 7);
        for (int k = max(0, x1 + x2 - 7); k <= min(x1 + x2, 7); k++) {
            if (x1 == k) num = cnt;
            cnt++;
        }


        for (int j = 0; j < 256; j++) {

            int x3 = x1;
            int x4 = x2;
            int num2 = num;

            ull x = 0;

            while (true) {
                x3--;
                x4++;
                num2--;
                if (num2 < 0) break;
                x += (1ull << (x3 * 8 + x4));
                if (((1ull << num2) & j) > 0) break;
            }

            num2 = num;
            x3 = x1;
            x4 = x2;

            while (true) {
                x3++;
                x4--;
                num2++;
                if (num2 > all) break;
                x += (1ull << (x3 * 8 + x4));
                if (((1ull << num2) & j) > 0) break;
            }

            dia1[i][j] = x;
        }

    }
    
    
    //препрочситываем все ходы по диагонали 2
        for (int i = 0; i < 64; i++) {

        int x1 = i / 8;
        int x2 = i % 8;

        int num = min(x1, x2);
        int all = 7 - abs(x1 - x2);

        for (int j = 0; j < 256; j++) {

            int x3 = x1;
            int x4 = x2;
            int num2 = num;

            ull x = 0;

            while (true) {
                x3--;
                x4--;
                num2--;
                if (num2 < 0) break;
                x += (1ull << (x3 * 8 + x4));
                if (((1ull << num2) & j) > 0) break;
            }

            num2 = num;
            x3 = x1;
            x4 = x2;

            while (true) {
                x3++;
                x4++;
                num2++;
                if (num2 > all) break;
                x += (1ull << (x3 * 8 + x4));
                if (((1ull << num2) & j) > 0) break;
            }
            dia2[i][j] = x;
        }

    }



}

int main() {

    cin.sync_with_stdio(false);
    cin.tie(0);
    
    cout.setf(ios::fixed);
    cout.precision(3);

    init();

    init_weights();
    
    srand(time(0));    

    vector < pair <ull, ull> > history;
    

    printfield();
    cout.flush();
    start = 9;
    int MARK = alphabeta(BLACK, start, -oo, +oo,1);
    cout << "ход: " << bb_to_str(best) << "\t"; 
    cout << " оценка за черных: " << 0.001*MARK << "\tстатическая оценка: " << -0.001*mark << endl;
    cout << "просмотрено позиций: " << 1.0*(cnt_ab+cnt_chop-cnt_ab_chop)/1000000 << "M, спокойных: " << 1.0*(cnt_ab)/1000000 << "M форсированных: " << 1.0*(cnt_chop-cnt_ab_chop)/1000000 << "M" << endl;
    return 0;
    
    
    printfield();
    cout.flush();
    for (int i = 0; i < 200; i++) {


        if (i == 80) {
            for (int j = 0; j < 80; j++) {

                vals[WHITE_KING][i] *= -1;
                vals[BLACK_KING][i] *= -1;
            }
        }

        int MARK;
        cout << "castle: " << castle.first << " " << castle.second << "\n";
        if (i % 2 == 0) {

            cout << "ход белых №" << i << "\n";            
            cout.flush();
            if (PLAYER1 > 0) {
                start = PLAYER1;
                MARK = alphabeta(WHITE, start, -oo, +oo,1);
                
                history.pb(best);
                make_move(best, 6);
                if (PRINT_MODE >= 2) cout << endl;
                cout << "ход: " << bb_to_str(best) << "\t";
                if (PRINT_MODE >= 1)
                cout << " оценка за белых: " << 0.001*MARK << "\tстатическая оценка: " << 0.001*mark;
                cout << endl;
            } else {
                string s1,s2;
                cout << "ваш ход (например: e2 e4 или O-O или O-O-O)" << endl;
                cin >> s1;
                if (s1 != "O-O" && s1 != "O-O-O") cin >> s2;
                
                if (s1 == "O-O") {
                    make_move(mp(100ull,100ull),222);
                } else
                if (s1 == "O-O-O") {
                    make_move(mp(200ull,200ull),222);
                } else
                make_move(move_to_bb(s1, s2), 3453);                
                
            }
        } else {
            cout << "ход черных №" << i << "\n";
            cout.flush();
            if (PLAYER2 > 0) {
                start = PLAYER2;
                MARK = alphabeta(BLACK, start, -oo, +oo,1);
     
                history.pb(best);
                make_move(best, 6);
                if (PRINT_MODE >= 2) cout << endl;
                cout << "ход: " << bb_to_str(best) << "\t";
                if (PRINT_MODE >= 1)
                cout << " оценка за черных: " << MARK << "\tстатическая оценка: " << -mark;
                cout << endl;
            } else {
                string s1,s2;
                cout << "ваш ход (например: e7 e5 или O-O или O-O-O)" << endl;
                cin >> s1;
                if (s1 != "O-O" && s1 != "O-O-O") cin >> s2;
                
                if (s1 == "O-O") {
                    make_move(mp(300ull,300ull),222);
                }  else
                if (s1 == "O-O-O") {
                    make_move(mp(400ull,400ull),222);
                } else
                make_move(move_to_bb(s1, s2), 3453);                
            }

        }
        
        printfield();
        cout.flush();



        if (abs(mark) > 200000) break;

    }


    

    return 0;

}
